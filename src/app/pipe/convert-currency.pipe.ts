import { Pipe, PipeTransform } from '@angular/core';
import {ServiceAuth} from "@services/api/auth.service";

@Pipe({
  name: 'convertCurrency',
  pure: false
})
export class ConvertCurrencyPipe implements PipeTransform {

  constructor(private auth: ServiceAuth) {}

  /**
   * Calculate currency
   * @param {string | number} value money
   * @param {string} currency currency convert to
   * @returns converted currency
   */
  transform(value: any, currency: string): any {
    if (typeof value === 'string') {
      value = parseInt(`${value}`.replace(/,/g, ''));
    }
    if (!currency) {
      return value;
    } else {
      let rates = this.auth?.currencyRates || {};
      // @ts-ignore
      return this.formatNumber(value / rates[currency]);
    }
  }

  formatNumber(num: number): string | number {
    if (num < 1e6) {
      return Math.round(num);
    } else if (num >= 1e6 && num < 1e9) {
      return (num / 1e6).toFixed(2) + 'M';
    } else {
      return (num / 1e9).toFixed(2) + 'B';
    }
  };
}

import { Directive, Input, ElementRef, HostListener, Renderer2 } from '@angular/core';
import * as $ from "jquery";
import 'slick-carousel';

@Directive({
  selector: '[slider]'
})
export class SliderDirective {
  @Input('slider') title!: string;
  @Input() placement = 'top';
  tooltip!: HTMLElement | null;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    setTimeout(() => {
      const e = this.el.nativeElement;
      const opt: any = {
        autoplaySpeed: 10000,
        infinite: true,
        speed: 1000,
        autoplay: true,
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        arrows: false,
        adaptiveHeight: false,
      };
      if (e.dataset['show']) {
        opt.slidesToShow = parseInt(e.dataset['show']);
        if (opt.slidesToShow > 2) {
          opt.responsive = [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: opt.slidesToShow < 5 ? 2 : opt.slidesToShow - 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: opt.slidesToShow < 5 ? 1 : opt.slidesToShow - 3,
              }
            }
          ];
          if (opt.slidesToShow > 3) {
            opt.responsive.unshift({
              breakpoint: 1281,
              settings: {
                slidesToShow: opt.slidesToShow < 5 ? 3 : opt.slidesToShow - 1,
              }
            })
          }
        }
      }
      if (!!e.dataset['dots']) {
        opt.dots = true;
      }
      if (!!e.dataset['arrows']) {
        opt.arrows = true;
      }
      if (!!e.dataset['adaptiveHeight']) {
        opt.adaptiveHeight = true;
      }
      if (!!e.dataset['appendArrows']) {
        opt.appendArrows = e.dataset['appendArrows'];
        opt.arrows = true;
      }
      if (!!e.dataset['asNavFor']) {
        opt.asNavFor = e.dataset['asNavFor'];
        $(opt.asNavFor).slick({
          speed: 1000,
          focusOnSelect: true,
          vertical: true,
          dots: false,
          arrows: false,
          slidesToScroll: 1,
          slidesToShow: 6,
          asNavFor: '#' + e.id,
          responsive: [
            {
              breakpoint: 640,
              settings: {
                vertical: false,
                slidesToShow: 4,
              }
            }
          ]
        });
      }
      $(e).slick(opt);
    }, )

  }
}

import {NgModule} from "@angular/core";
import {TooltipDirective} from "./tooltip/tooltip.directive";
import {SliderDirective} from "./slider/slider.directive";

@NgModule({
  exports: [TooltipDirective, SliderDirective],
  declarations: [TooltipDirective, SliderDirective]
})
export class SharedDirective {}

import {AuthResponseData} from "@services/api/auth.service";

export class Auth {
  public userId: string;
  public userModel: AuthResponseData;
  public tokenString: string;
  public issuedAt: Date;
  public expiresAt: Date;
  public listRole: {id: string, code: string, name: string, isSystem: boolean}[];
  public appSettings: {
    reloadOrderListAfterOrderActions: string[],
    reloadMenuAfterOrderActions: string[],
    productListStyle: string,
  };

  constructor(
    userId: string,
    userModel: AuthResponseData,
    tokenString: string,
    issuedAt: Date,
    expiresAt: Date,
    listRole: {id: string, code: string, name: string, isSystem: boolean}[],
    appSettings: {
      reloadOrderListAfterOrderActions: string[],
      reloadMenuAfterOrderActions: string[],
      productListStyle: string,
    },
  ) {
    this.userId = userId;
    this.userModel = userModel;
    this.tokenString = tokenString;
    this.issuedAt = issuedAt;
    this.expiresAt = expiresAt;
    this.listRole = listRole;
    this.appSettings = appSettings;
  }

  get token(): string {
    if (!this.expiresAt || new Date() > this.expiresAt) {
      return ''
    }
    return this.tokenString;
  }
}

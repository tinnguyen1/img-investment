import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject, throwError} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '@src/environments/environment';

import { Auth } from '@models/auth.model';
import {ServiceMessage} from "@services/message.service";

export interface AuthResponseData {
  applicationId: string;
  avatarUrl: string;
  birthdate: Date;
  email: string;
  id: string;
  isLockedOut: boolean;
  lastActivityDate: Date;
  level: number;
  name: string;
  phoneNumber: string;
  type: number;
  userName: string;
  countryCode: string;
  currency: string;
  language: string;
  partner: any;
}
export interface AuthData {
  userId: string;
  userModel: AuthResponseData;
  tokenString: string;
  issuedAt: Date;
  expiresAt: Date;
  listRole: {id: string, code: string, name: string, isSystem: boolean}[];
  appSettings: {
    reloadOrderListAfterOrderActions: string[],
    reloadMenuAfterOrderActions: string[],
    productListStyle: string,
  };
}
export interface AuthRequestData {
  loginName: string;
  password: string;
  name?: string;
  rememberMe?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ServiceAuth {
  public user = new BehaviorSubject<Auth>(new Auth('',{
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "userName": "string",
    "name": "string",
    "email": "string",
    "avatarUrl": "string",
    "phoneNumber": "string",
    "type": 0,
    "birthdate": new Date(),
    "lastActivityDate": new Date(),
    "applicationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "level": 0,
    "isLockedOut": true,
    countryCode: "VN",
    language: "VN",
    currency: "USD",
    partner: null,
  },'', new Date(), new Date(), [], {
    "reloadOrderListAfterOrderActions": [
      "string"
    ],
    "reloadMenuAfterOrderActions": [
      "string"
    ],
    "productListStyle": "string"
  }));
  public currency$ = new BehaviorSubject<string>('USD');
  public isShowLogin$ = new BehaviorSubject<boolean>(false);
  public isShowModalAuth$ = new BehaviorSubject<boolean>(false);

  private tokenExpirationTimer: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    protected message: ServiceMessage,
  ) {
  }

  currencyRates: any[] = [];

  currencyRate(): any {
    return this.http
      .get<any>(`${environment.adminApiUrl}/api/v1/currencies/${this.currency$.value}/rates`)
      .pipe(catchError(err => this.handleError(err)))
      .subscribe(rs => {
        if (rs.success) { this.currencyRates = rs.rates; }
      });
  }

  autoLogin(): void {
    const data: AuthData = localStorage.getItem(environment.userData) && JSON.parse(localStorage.getItem(environment.userData) || '');
    if (!data) {
      return;
    }
    const loadedUser = new Auth(data.userId, data.userModel, data.tokenString, data.issuedAt, data.expiresAt, data.listRole, data.appSettings);

    if (loadedUser.tokenString) {
      this.user.next(loadedUser);
    }
  }

  signUp(data: {email?: string, password?: string}): any {
    return this.http
      .post<AuthResponseData>(environment.adminApiUrl + '/api/v1/idm/users/register', data)
      .pipe(
        catchError(err => this.handleError(err)),
        tap((res: any) => {
          this.message.success(res.message);
          // return this.handleAuthentication(res.data);
        })
      );
  }

  signIn(data: {email?: string, password?: string}): any {
    return this.http
      .post<AuthData>(environment.adminApiUrl + '/api/v1/authentication/jwt/login', data)
      .pipe(
        catchError(err => this.handleError(err)),
        tap((res: any) => {
          return this.handleAuthentication(res.data);
        })
      );
  }

  fbLogin(data: any): any {
    return this.http
      .post<AuthData>(environment.adminApiUrl + '/api/v1/authentication/fb/login', data)
      .pipe(
        catchError(err => this.handleError(err)),
        tap((res: any) => {
          return this.handleAuthentication(res.data);
        })
      );
  }

  googleLogin(data: any): any {
    return this.http
      .post<AuthData>(environment.adminApiUrl + '/api/v1/authentication/gg/login', data)
      .pipe(
        catchError(err => this.handleError(err)),
        tap((res: any) => {
          return this.handleAuthentication(res.data);
        })
      );
  }

  forgotPass(phoneNumber: string): any {
    return this.http
      .put<AuthResponseData>(environment.adminApiUrl + '/api/v1/idm/users/forgotpassword/' + phoneNumber, {})
      .pipe(
        catchError(err => this.handleError(err)),
        tap((res: any) => this.message.success(res.message))
      );
  }

  logout(): void {
    this.router.navigate(['/']);
    localStorage.removeItem(environment.userData);
    setTimeout(() => window.location.reload())
  }

  autoLogout(expirationDuration: number): void {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  handleError(err: HttpErrorResponse): any {
    if (err.error.message) {
      this.message.error(err.error.message);
    }
    return throwError(err.error);
  }

  handleAuthentication(data: AuthData): void {
    const user = new Auth(data.userId, data.userModel, data.tokenString, data.issuedAt, data.expiresAt, data.listRole, data.appSettings);
    user.userModel.language = user.userModel.language.toUpperCase();
    this.user.next(user);
    localStorage.setItem(environment.userData, JSON.stringify(data));
  }

  get userAuth(): AuthData | null {
    const auth = localStorage.getItem(environment.userData);
    if (!auth) { return null; }
    return JSON.parse(auth);
  }
}

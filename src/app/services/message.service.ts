import Swal from 'sweetalert2';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ServiceMessage {
  constructor() {
  }

  success(message: string) : any {
    return Swal.fire({
      icon: 'success',
      text: message,
      showConfirmButton: false,
      timer: 1500
    })
  }

  error(message: string) : any {
    return Swal.fire({
      icon: 'error',
      text: message,
    })
  }
}

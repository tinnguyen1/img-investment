import * as echarts from 'echarts';
import '/src/assets/map/world';

export class ServiceChart {
  constructor() {
  }

  map({id}: {id: string}) : any {
    setTimeout(() => {
      const element = document.getElementById(id);
      if (element) {
        var worldGeoCoordMap: any = {
          'United States': [ -74.006111,40.712778 ],
          'Serbia': [20.466667,44.816667 ],
          'Paraguay': [-57.641389, -25.296389],
          'India': [77.209006, 28.613895],
          'Korea': [126.99, 37.56],
          'Japan': [139.692222, 35.689722],
          'Australia': [149.126944, -35.293056],
          'Vietnam': [105.853333, 21.028333],
        };
        var worldDatas = [
          [{
            name: 'United States',
            value: 0
          }],	[{
            name: "Serbia",
            value: 0
          }],
          [{
            name: 'Paraguay',
            value: 0
          }],
          [{
            name: 'India',
            value: 0
          }],
          [{
            name: 'Korea',
            value: 0
          }],
          [{
            name: 'Japan',
            value: 0
          }],
          [{
            name: 'Australia',
            value: 0
          }],
          [{
            name: 'Vietnam',
            value: 0
          }],
        ];

        var convertData = function(data:any) {
          var res = [];
          for(var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            var fromCoord = worldGeoCoordMap[dataItem[0].name];
            var toCoord = worldGeoCoordMap['Vietnam'];
            if(fromCoord && toCoord) {
              res.push([{
                coord: fromCoord,
                value: dataItem[0].value
              }, {
                coord: toCoord,
              }]);
            }
          }
          return res;
        };
        var series: any = [];
        [['Vietnam', worldDatas]].forEach(function(item: any, i) {
          series.push(
            {
              type: 'lines',
              zlevel: 2,
              effect: {
                show: true,
                period: 4,
                trailLength: 0.02,
                symbol: 'arrow',
                symbolSize: 5,
              },
              lineStyle: {
                width: 1,
                opacity: 1,
                curveness: .7,
                color: '#F49B99'
              },
              data: convertData(item[1])
            },
            {
              type: 'effectScatter',
              coordinateSystem: 'geo',
              zlevel: 2,
              rippleEffect: {
                period: 4,
                brushType: 'stroke',
                scale: 4
              },
              label: {
                normal: {
                  show: true,
                  position: 'right',
                  offset: [5, 0],
                  formatter: function(params: any){
                    return params.data.name;
                  },
                  fontSize: 13
                },
                emphasis: {
                  show: true
                }
              },
              symbol: 'circle',
              symbolSize: function(val: any) {
                return 5+ val[2] * 5;
              },
              itemStyle: {
                normal: {
                  show: false,
                  color: '#e92f31'
                }
              },
              data: item[1].map(function(dataItem: any) {
                return {
                  name: dataItem[0].name,
                  value: worldGeoCoordMap[dataItem[0].name].concat([dataItem[0].value])
                };
              }),
            },
            // {
            //   type: 'scatter',
            //   coordinateSystem: 'geo',
            //   zlevel: 2,
            //   rippleEffect: {
            //     period: 4,
            //     brushType: 'stroke',
            //     scale: 4
            //   },
            //   label: {
            //     normal: {
            //       show: true,
            //       position: 'right',
            //       color: '#0f0',
            //       formatter: '{b}',
            //       textStyle: {
            //         color: "#0f0"
            //       }
            //     },
            //     emphasis: {
            //       show: true,
            //       color: "#f60"
            //     }
            //   },
            //   symbol: 'pin',
            //   symbolSize: 50,
            //   data: [{
            //     name: item[0],
            //     value: worldGeoCoordMap[item[0]].concat([10]),
            //   }],
            // }
          );
        });

        echarts.init(element).setOption({
          grid: {
            top: '0',
            left: '0',
            bottom: '0',
            right: '0',
          },
          // tooltip : {
          //   trigger: 'item',
          //   formatter : function (params: any) {
          //     return params.name;
          //   }
          // },
          toolbox: {
            show : false,
          },
          geo: {
            map: 'world',
            zoom: 1.2,
            label: {
              emphasis: {
                show: false
              }
            },
            roam: false,
            itemStyle: {
              color: '#576080',
              borderColor: '#A2A8BF',
              borderWidth: 0
            }
          },
          series : series
        });
      }
    }, );
  }

  bar(
    {id, label, series, tooltip = {}, grid = {}, legend = {}, xAxis = {}}: {
      id: string,
      label: string[],
      series: {
        name: string,
        type: string,
        barWidth?: string,
        showBackground?: boolean,
        itemStyle: {
          color: string,
          borderRadius?: number,
        },
        data: number[],
      }[],
      tooltip?: {
        backgroundColor?: string,
        textStyle?: {
          color?: string
        }
      },
      grid?: {
        top?: string,
        left?: string,
        bottom?: string,
        right?: string,
      },
      legend?: {
        show?: boolean,
        textStyle?: {
          color?: string,
        }
      },
      xAxis?: {
        show?: boolean,
      }
    }): any {
    setTimeout(() => {
      const element = document.getElementById(id);
      if (element) {
        echarts.init(element).setOption({
          tooltip: {
            trigger: 'axis',
            backgroundColor: '#8A92AE',
            textStyle: { color: '#ffffff' },
            ...tooltip,
          },
          grid: {
            top: '30px',
            bottom: '30px',
            left: '30px',
            right: '0',
            ...grid,
          },
          legend: {
            show: true,
            textStyle: { color: "#8A92AE" },
            ...legend,
          },
          xAxis: {
            type: 'category',
            boundaryGap: true,
            axisLabel: {
              show: true,
            },
            axisLine: {
              show: true,
              lineStyle: {
                color: '#8A92AE',
              }
            },
            splitLine: {
              show: false,
            },
            axisTick: {
              show: true,
              lineStyle: {
                color: '#8A92AE',
              }
            },
            ...xAxis,
            data: label,
          },
          yAxis: {
            show: true,
            type: 'value',
          },
          series
        });
      }
    });
  }
}

import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ServiceChart} from "@services/echart.service";

@Component({
  selector: 'app-device',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [ServiceChart]
})
export class HomeComponent implements OnInit,OnDestroy {
  constructor(
  ) {
  }
  ngOnInit(): void {
  }
  ngOnDestroy(): void {
  }
}

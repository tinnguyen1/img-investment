import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-geneat-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit {
  @Input() loading?: boolean;
  @Input() title?: string;
  @Input() visible = false;
  @Input() contentModal: any;
  @Input() extendFooterButton: any;
  @Input() width = 600;
  @Input() notFooter = false;
  @Input() okDisabled = false;
  @Input() okHidden = true;
  @Input() keyboard = false;
  @Input() classModal = '';

  @Output() clickOk = new EventEmitter();
  @Output() clickCancel = new EventEmitter();

  @Input() okText?: string;

  constructor() { }

  ngOnInit(): void {
  }

  handleCancel(): void {
    this.clickCancel.emit();
  }

  handleOk(): void {
    this.clickOk.emit();
  }
}

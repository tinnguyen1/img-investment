import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class MainComponent implements OnInit, OnDestroy {
  constructor(
    protected route: ActivatedRoute,
    protected translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.translateService.onDefaultLangChange.subscribe((langChangeEvent: LangChangeEvent) => {
      document.body.className = langChangeEvent.lang;
    });
  }

  ngOnDestroy(): void {
  }
}

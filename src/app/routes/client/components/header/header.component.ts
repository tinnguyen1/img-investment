import {Component, OnInit, ViewEncapsulation, OnDestroy, ElementRef, ViewChild} from '@angular/core';
import {BaseForm} from "@routes/client/base/form";
import { HostListener } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  encapsulation: ViewEncapsulation.None,
  providers: [],
})

export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild('scroll')
  private scroll!: ElementRef;
  constructor(
  ) {
  }

  ngAfterViewInit() {
  }

  ngOnInit(): void {
    window.addEventListener('scroll',(event) => {
      if(scrollY == 0){
        this.scroll.nativeElement.classList.add('bg-transparent');
        this.scroll.nativeElement.classList.remove('bg-gray-800');
      } else {
        this.scroll.nativeElement.classList.add('bg-gray-800');
        this.scroll.nativeElement.classList.remove('bg-transparent');
      }
    });
  }

  ngOnDestroy(): void {
  }
}

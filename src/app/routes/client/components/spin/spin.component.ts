import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-spin',
  templateUrl: './spin.component.html',
  styleUrls: ['./spin.component.less'],
})
export class SpinComponent implements OnInit {

  @Input() loading = false;

  constructor() { }

  ngOnInit(): void {
  }
}

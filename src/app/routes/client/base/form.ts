import {Injectable} from '@angular/core';


@Injectable()
export abstract class BaseForm {

  public messagesErrorForm: any = {}

  protected constructor(
  ) {}

  getErrorMessage(control: any) {
    const formGroup = control._parent.controls;
    const name =  Object.keys(formGroup).find(name => control === formGroup[name]) || '';
    if (this.messagesErrorForm.hasOwnProperty(name)) {
      for (const propertyName in control.errors) {
        if (control.errors.hasOwnProperty(propertyName)) {
          switch (propertyName) {
            case 'min':
              return this.messagesErrorForm[name][propertyName]?.replace('{min}', control.errors[propertyName].min);
            case 'max':
              return this.messagesErrorForm[name][propertyName]?.replace('{max}', control.errors[propertyName].max);
            default:
              return this.messagesErrorForm[name][propertyName];
          }
        }
      }
    }
    return null;
  }
}

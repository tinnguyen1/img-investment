import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClientRouting} from './client.routing';
import {ClientComponent} from './client.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

import {SharedDirective} from "@src/app/directive/shared.directive";
import {PaginationComponent} from './components/pagination/pagination.component';
import {FullTextSearchPipe} from "@src/app/pipe/full-text-search.pipe";
import { ConvertCurrencyPipe } from "@src/app/pipe/convert-currency.pipe";
import { SpinComponent } from './components/spin/spin.component';

import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import {NzFormModule} from "ng-zorro-antd/form";

import { NzDropDownModule } from 'ng-zorro-antd/dropdown';

import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {MainComponent} from './components/main/main.component';

import {HomeComponent} from './pages/home/home.component';
import {AuthInterceptor} from "@src/app/auth.interceptor";
import {GeneatModalModule} from "@routes/client/components/modal/modal.module";
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { OurServiceComponent } from './pages/our-service/our-service.component';
import { MmatchDetailComponent } from './pages/mmatch-detail/mmatch-detail.component';
import { MfundDetailComponent } from './pages/mfund-detail/mfund-detail.component';
import { OurPartnerComponent } from './pages/our-partner/our-partner.component';
import { PartnerDetailComponent } from './pages/partner-detail/partner-detail.component';
import { OurEventsComponent } from './pages/our-events/our-events.component';
import { EventsDetailComponent } from './pages/events-detail/events-detail.component';
import { OurNewsComponent } from './pages/our-news/our-news.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { SearchComponent } from './pages/search/search.component';

@NgModule({
  declarations: [
    ConvertCurrencyPipe,
    FullTextSearchPipe,
    ClientComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    HomeComponent,
    PaginationComponent,
    SpinComponent,
    AboutUsComponent,
    OurServiceComponent,
    MmatchDetailComponent,
    MfundDetailComponent,
    OurPartnerComponent,
    PartnerDetailComponent,
    OurEventsComponent,
    EventsDetailComponent,
    OurNewsComponent,
    ContactUsComponent,
    SearchComponent,
  ],
  imports: [
    SharedDirective,
    CommonModule,
    ClientRouting,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: localStorage.getItem('ng-language') || 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
          return new TranslateHttpLoader(http, 'assets/translations/');
        },
        deps: [HttpClient]
      }
    }),
    FormsModule,
    NzSelectModule,
    NzDropDownModule,
    NzTabsModule,
    NzRadioModule,
    NzSliderModule,
    NzSwitchModule,
    NzCheckboxModule,
    NzFormModule,
    ReactiveFormsModule,
    GeneatModalModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
})
export class ClientModule {
}

import { NgModule, Component } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MainComponent} from './components/main/main.component';
import {HomeComponent} from './pages/home/home.component';
import {AboutUsComponent} from './pages/about-us/about-us.component';
import {OurServiceComponent} from './pages/our-service/our-service.component'
import {MmatchDetailComponent} from './pages/mmatch-detail/mmatch-detail.component'
import {MfundDetailComponent} from './pages/mfund-detail/mfund-detail.component'
import {OurPartnerComponent} from './pages/our-partner/our-partner.component'
import {PartnerDetailComponent} from './pages/partner-detail/partner-detail.component'
import {OurEventsComponent} from './pages/our-events/our-events.component'
import {EventsDetailComponent} from './pages/events-detail/events-detail.component'
import {OurNewsComponent} from './pages/our-news/our-news.component'
import {ContactUsComponent} from './pages/contact-us/contact-us.component'
import {SearchComponent} from './pages/search/search.component'



const childrenRoutes: Routes = [
  { path: '', component: HomeComponent },
  // { path: '**', redirectTo: '', pathMatch: 'full' },
  { path: 'about-us', component: AboutUsComponent},
  { path: 'our-service', component: OurServiceComponent},
  { path: 'mmatch-detail', component: MmatchDetailComponent},
  { path: 'mfund-detail', component: MfundDetailComponent},
  { path: 'our-partner', component: OurPartnerComponent},
  { path: 'partner-detail', component: PartnerDetailComponent},
  { path: 'our-events', component: OurEventsComponent},
  { path: 'events-detail', component: EventsDetailComponent},
  { path: 'our-news', component: OurNewsComponent},
  { path: 'contact-us', component: ContactUsComponent},
  { path: 'search', component: SearchComponent}
];

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: childrenRoutes
  },
  {
    path: ':language',
    component: MainComponent,
    children: childrenRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRouting {
}
